﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Soln_DDL_Task1.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Task 1</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"/>
</head>
<body>
    <form id="form1" runat="server">
    <div>
       
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
       
    </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table class="w3-table-all" style="width:400px; margin-left: auto;  margin-right: auto; height:200px" >
                    <tr>
                        <td>
                               <asp:TextBox ID="txtvalue" CssClass="w3-input w3-animate-input" runat="server" placeHolder="Text" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnsubmit" runat="server" CssClass="w3-button" Text="Submit" OnClick="btnsubmit_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:DropDownList ID="ddl1" runat="server" CssClass="w3-dropdown-hover w3-button " />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblerror" runat="server" />
                        </td>
                    </tr>
                  
                </table>
               
              
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
